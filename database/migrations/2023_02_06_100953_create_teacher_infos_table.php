<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher_infos', function (Blueprint $table) {
            $table->id('teacher_id');
            $table->string('name', 20);
            $table->string('lab', 15);
            $table->string('fax', 25);
            $table->string('telephone', 25);
            $table->string('email', 32);
            $table->text('officeHour');
            $table->text('degree');
            $table->text('expertise');
            $table->text('personalWebsite');
            $table->text('imgUrl');
            $table->string('position', 15);
            $table->string('status', 10);
            $table->timestamps();           
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher_infos');
    }
};
