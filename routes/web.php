<?php

use App\Http\Controllers\adminController;
use App\Http\Controllers\FileUploadController;
use App\Http\Controllers\ndhu008\professorController;
use App\Http\Controllers\ndhu011\aboutusController;
use App\Http\Controllers\ndhu100\articleManagerController;
use App\Http\Controllers\ndhu101\outerArticleManagerController;
use App\Http\Controllers\ndhu194\imgUploadController;
use App\Http\Controllers\ndhu195\docsUploadController;
use App\Http\Controllers\ndhu196\searchController;
use App\Http\Controllers\ndhu197\categoriesDetailController;
use App\Http\Controllers\ndhu198\postController;
use App\Http\Controllers\ckfindertest;
use App\Http\Controllers\ndhu199\homepageController;
use App\View\Components\ndhu008\professorsCard;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [
    homepageController::class, 'getHomepagePosts'
])->middleware('guest')->name('homepage');

Route::get('/dashboard', [
    homepageController::class, 'getHomepagePosts'
])->middleware(['auth'])->name('dashboard');

Route::prefix('category')->group(function () {
    /*
    依據網址呼叫 controller 方法 全部貼文存在一張表 有 randompost id
    有分類 category
    */
    // category -> 在校生專區(student) -> categoryName
    // navtab categories more
    Route::get('{categoryBig}/{categoryName}',[categoriesDetailController::class, 'getCategoriesMorePosts']);
    Route::get('/posts/{tableName}/{postRandomId}', [postController::class, 'getPost']);
});

Route::get('/getPostContent/aboutus/{category}', [aboutusController::class, 'getAboutusContent']);
Route::get('/getPostContent/{tableName}/{postRandomId}', [postController::class, 'getPostContent']);
Route::get('/getPostContent/dashboard/{editstatus}/aboutus/{category}', [aboutusController::class, 'getAboutusContent']);
Route::get('/getPostContent/dashboard/aboutus/{category}', [aboutusController::class, 'getAboutusContent']);
Route::post('/postPostContent/aboutus', [aboutusController::class, 'posteditaboutus']);

Route::prefix('staff')->group(function () {
    Route::get('/{staffType}', [professorController::class, 'getStaffs'])->middleware('guest');
});
Route::get('/aboutus/{category}', [aboutusController::class, 'getAboutus'])->middleware('guest');

Route::middleware('auth')->prefix('dashboard')->group(function () {
    Route::get('/add-post/{tableName}', [articleManagerController::class, 'getAddPost'])->name("addPost");
    Route::get('/add-post-outer/{tableName}', [outerArticleManagerController::class, 'getAddPostOuter'])->name("addPostOuter");

    //use js fetch
    Route::post('/add-post/{tableName}', [articleManagerController::class, 'postAddPost']);
    Route::post('/add-post-outer/{tableName}', [outerArticleManagerController::class, 'postAddPostOuter']);

    Route::get('/edit-post/{tableName}/{postRandomId}', [articleManagerController::class, 'getEditPost'])->name("editPost");
    Route::get('/edit-post-outer/{tableName}/{postRandomId}', [outerArticleManagerController::class, 'getEditPostOuter'])->name("editPostOuter");

    //use js fetch
    Route::post('/edit-post/{tableName}/{postRandomId}', [articleManagerController::class, 'postEditPost']);
    Route::post('/edit-post-outer/{tableName}/{postRandomId}', [outerArticleManagerController::class, 'postEditPostOuter']);

    Route::delete('/delete-post/{tableName}/{postRandomId}', [articleManagerController::class, 'deletePost']);

    Route::post('receiveImg/{imgNum}', [imgUploadController::class, 'recImg']);
    Route::post('postDocs/{docNum}', [docsUploadController::class, 'postDocs']);

    Route::get('/aboutus/{category}', [aboutusController::class, 'getAboutus']);

    Route::get('/editaboutus/aboutus/{category}', [aboutusController::class, 'geteditaboutus'])->name('getEditAboutusContent');
    Route::post('/editaboutus/aboutus/{category}', [aboutusController::class, 'posteditaboutus'])->name('postEditAboutusContent');
});

Route::middleware('auth')->prefix('dashboard/category')->group(function () {
    Route::get('{categoryBig}/{categoryName}',[categoriesDetailController::class, 'getCategoriesMorePosts']);
    Route::get('/professors', [professorController::class, 'getProfessors']);
});

Route::get('/search', [searchController::class, 'searchPosts']);
Route::post('/ckfinder/connector', [ckfindertest::class, 'imgprocess']);

// 引入 routes/auth.php 下面的 middleware, get, post 都可使用
require __DIR__ . '/auth.php';