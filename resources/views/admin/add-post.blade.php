<x-post-edit>
    <x-slot name="headRes">
        <link rel="stylesheet" href="{{ asset('css/NDHU011/NDHU011.css') }} " />
        {{-- <link rel="stylesheet" href="{{ asset('css/NDHUUTILS/NDHUUTILS_001008.css') }} " /> --}}
        </x-slot>
    <div id="uploadCtrlPanel">
        <h2>請輸入文章標題</h2>
        <input id="postTitleInput" name="postTitleInput" value="{{$postTitle}}" placeholder="請輸入標題">
        <button id="postTitleBtn">確定</button>
        {{-- <h2>新增段落</h2> --}}
        {{-- <div id="paraAndImgAdder"> --}}
            {{-- <textarea id="textarea" name="textarea"></textarea> --}}
            {{-- <button id="addParagraph">新增段落</button> --}}
            {{-- <button id="addImg">新增圖片</button> --}}
        {{-- </div> --}}      
        <h2>上傳文件</h2>
        <button id="addDoc">新增文件</button>  
    </div>
    <hr>
    <div id="postTitleContainer"></div>
    <x-ndhuckeditor.ndhuckeditor :editorContent="$textarea"></x-ndhuckeditor.ndhuckeditor>
    <div>
        {{-- <div id="postContainer"> --}}
            {{-- @php $textarea = str_replace("\"", "\'", $textarea) @endphp
            <?= $textarea ?> --}}
        {{-- </div> --}}
        <h2>附件一覽</h2>
        <div id="fileUploadArea">
            <?= $before_uploadFiles ?>
        </div>
    </div>
    <x-slot name="scriptsRes">
        {{-- <script src="{{ asset('js/outsources/ckfinder.js')}}"></script> --}}
        <script src="{{ asset('js/outsources/ckeditor.js')}}"></script>
        <script src="{{ asset('js/NDHUUTILS/NDHUUTILS_ckeditor.js') }}"></script>
    </x-slot>
</x-post-edit>