<x-post-edit>
    <div>
        <input type="text" name="linkTitle" id="linkTitle" value="{{$linkTitle}}" placeholder="請輸入文章標題">
        <input type="text" name="linkInput" id="linkInput" value="{{$linkUrl}}" placeholder="請輸入文章連結">
        <button id="linkInputCkBtn">確定</button>
    </div>

    <x-slot name="scriptsRes">
        <script src="{{ asset('js/NDHU001/NDHU001.js') }}"></script>
    </x-slot>
</x-post-edit>