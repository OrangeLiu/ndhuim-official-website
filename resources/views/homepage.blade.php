<x-app-layout>
    {{-- @include('./util/notLogin') --}}
    {{-- @include('./util/isLogin') --}}
    <x-slot name="headRes">
    <link rel="stylesheet" href="{{ asset('css/NDHU002/NDHU002.css') }} " />
    <link rel="stylesheet" href="{{ asset('css/NDHU005/NDHU005.css') }} " />
    <link rel="stylesheet" href="{{ asset('css/NDHU006/NDHU006.css') }} " />
    <link rel="stylesheet" href="{{ asset('css/NDHU010/NDHU010.css') }} " />
    <link rel="stylesheet" href="{{ asset('css/outsources/owl.carousel.min.css') }} " />
    <link rel="stylesheet" href="{{ asset('css/outsources/owl.theme.default.min.css') }} " />
    </x-slot>
    
        {{-- navtab --}}
        <div>
            <div>
                <x-ndhu003.nav-tab 
                :latestnews="$latestnews"
                :speechPosts="$speechPosts"
                :enrollment="$enrollment"
                :honor="$honor"
                :course="$course"
                ></x-ndhu003.nav-tab>                
            </div>
            <div>
                <h2>熱門搜尋</h2>
            </div>
        </div>

        {{-- honor --}}
        <x-ndhu005.honor-element :honorPosts="$honor"></x-ndhu005.honor-element>
        {{-- studentArea --}}
        <x-ndhu006.ndhu006></x-ndhu006.ndhu006>
        {{-- googlemap --}}
        <x-ndhu010.ndhu010></x-ndhu010.ndhu010>

    <x-slot name="scriptsRes">
    @if(Route::is('homepage'))
    <script src="{{ asset('js/NDHU001/NDHU001.js') }}"></script>
    @endif
    <script src="{{ asset('js/NDHU002/NDHU002.js') }}"></script>
    <script src="{{ asset('js/NDHU005/NDHU005.js') }}"></script>
    <script src="{{ asset('js/outsources/owl.carousel.min.js') }}"></script>
    </x-slot>
</x-app-layout>