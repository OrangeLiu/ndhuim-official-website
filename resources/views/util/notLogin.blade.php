<x-slot name="welcome">
    <div id="main">
        <div id="main-1">
            <div id="main-mid">
                @if (isset($posts))
                @foreach ($posts as $post)
                <a href={{$post->Url}}>{{ $post->title }}</a>
                @endforeach
                {{-- @endisset --}}
                @elseif (isset($category))
                @forelse ($category as $k => $v)
                <a href={{$categoryLink[$k]}}>{{ $v }}</a>
                @empty
                <p>No Posts</p>
                @endforelse
                @endif
                {{-- @include('util\category') --}}
            </div>
            <div id="main-right">
                <iframe
                    src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FNDHUIMSA%2F&tabs=timeline&width=250&height=300&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId"
                    width="250" height="250" style="border: none; overflow: hidden" scrolling="no" frameborder="0"
                    allowfullscreen="true"
                    allow="autoplay; clipboard-write; encrypted-media; picture-in-picture; web-share"></iframe>
            </div>
        </div>
        <div id="main-2">
            <div id="main-left">
                <ul>
                    <li>未來學生</li>
                    <li>碩士相關</li>
                    <li>新生看這邊</li>
                    <li>五年一貫碩士直升</li>
                    <li>大學部畢業專題展</li>
                </ul>
            </div>
        </div>
    </div>
</x-slot>