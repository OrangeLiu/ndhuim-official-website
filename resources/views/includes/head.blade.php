<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="NDHUIM Official Website" />
    <link rel="stylesheet" href="{{ asset('css/mobileNav.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/main.css') }} "/>
    <link rel="stylesheet" href="{{ asset('css/index.css') }} "/>
    <!-- <link rel="icon" href="orange.ico" /> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/all.min.css" integrity="sha512-KfkfwYDsLkIlwQp6LFnl8zNdLGxu9YAA1QvwINks4PhcElQSvqcyVLLD9aMhXd13uQjoXtEKNosOWaZqXgel0g==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.1/css/svg-with-js.min.css" integrity="sha512-U7WyVKwgyoYSa+qowujpUQIH3omU6SlFFr8m6kiEuuM1lWqoiURgTNskMFEf1la4PDNQzMws/G1u0wKGNxVbcQ==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <title>國立東華大學 資訊管理學系暨研究所 | NDHUIM</title>
</head>

<body>
    <header>
        <h1 id="logo"><a href="http://134.208.30.66">NDHUIM</a></h1>
        <nav class="nav-bar">
            <button id="side-menu-toggle">Menu</button>
            <ul class="nav-parent-headbar">
                <li><a id="l1" href="http://134.208.30.66/category/futurestudent-zh_tw/">未來學生</a>

                </li>
                <li><a id="l2" href="http://134.208.30.66/category/newslist-zh_tw" rel="bookmark">系所公告</a>
                    <ul class="nav-child-bar">
                        <li><a>所有公告</a></li>
                        <li><a>演講公告</a></li>
                        <li><a>研討會訊息</a></li>
                        <li><a>招生訊息</a></li>
                        <li><a>競賽公告</a></li>
                    </ul>
                </li>
                <li><a id="l3" href="http://134.208.30.66/category/aboutus-zh_tw">系所資訊</a></li>
                <li><a id="l4" href="#">學術研究與成果</a></li>
                <li><a id="l5" href="#">課程資訊</a></li>
                <li><a id="l6" href="#">學生活動</a></li>
                <li><a id="l7" href="#">線上資源</a></li>
                <li><a id="l8" href="../en/index.php">English</a></li>
                @guest
                    <li><a href="http://134.208.30.66/login">登入</a></li>                    
                @endguest
                @auth
                    <li><a>username</a></li>
                @endauth
                <li><button id="l9" href="#">搜尋</button></li>
            </ul>
            <form action="http://134.208.30.66/search" method="get">
                @csrf
                <input type="text" name="s_t" placeholder="請輸入關鍵字" />
                <input style="display: none;" type="submit" name="search" value="Search" />
            </form>
        </nav>
    </header>
    <main>
        <div id="main">
            <div id="main-1">
                <div id="main-mid">