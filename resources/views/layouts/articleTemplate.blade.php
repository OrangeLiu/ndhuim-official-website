<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', '國立東華大學資訊管理學系暨研究所') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    <link rel="stylesheet" href="{{ asset('css/index.css') }} " />

    {{-- header & footer --}}
    <link rel="stylesheet" href="{{ asset('css/NDHU001/NDHU001.css') }} " />
    <link rel="stylesheet" href="{{ asset('css/NDHU007/NDHU007.css') }} " />
    {{$headRes}}
    {{-- @vite(['resources/css/app.css', 'resources/js/app.js']) --}}
</head>

<body class="font-sans">
    {{-- header --}}
    <x-ndhu001.ndhu001></x-ndhu001.ndhu001>
    <div style="padding-bottom: 8vh" class="font-sans text-gray-900 antialiased">
        {{ $slot }}
    </div>

    <x-ndhu007.ndhu007></x-ndhu007.ndhu007>
    {{$scriptsRes}}
    {{-- <script src="{{ asset('resources/js/') }}"></script> --}}
</body>

</html>