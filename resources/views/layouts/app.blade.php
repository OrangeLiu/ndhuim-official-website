<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', '國立東華大學資訊管理學系暨研究所') }}</title>

    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">

    <!-- Scripts -->
    {{-- <link rel="stylesheet" href="{{ asset('css/mobileNav.css') }}" /> --}}
    <link rel="stylesheet" href="{{ asset('css/main.css') }} " />
    <link rel="stylesheet" href="{{ asset('css/index.css') }} " />

    {{-- tailwind --}}
    <link rel="stylesheet" href="{{ asset('css/tailwind.css') }} " />

    {{-- other head resources --}}
    {{$headRes}}

    {{-- header & footer --}}
    <link rel="stylesheet" href="{{ asset('css/NDHU001/NDHU001.css') }} " />
    <link rel="stylesheet" href="{{ asset('css/NDHU007/NDHU007.css') }} " />
    <!-- bootstrap -->
    <link rel="stylesheet" href="{{ asset('css/outsources/bootstrapv51.min.css') }} " />
    {{-- @vite(['public/css/app.css','public/js/app.js']) --}}
</head>

<body class="font-sans">
    {{-- header --}}
    <div class="">
        <x-ndhu001.ndhu001></x-ndhu001.ndhu001>
        {{-- @include('/ndhu001/ndhu001') --}}
    
        {{-- carousel --}}
        @if(Route::is('homepage'))
        <x-ndhu002.ndhu002></x-ndhu002.ndhu002>
        @endif
    
        <main>
            {{-- @guest
            {{$guest}}
            @endguest
            @auth
            {{$dashboard}}
            @endauth --}}
            {{$slot}}
        </main>
        
        {{-- footer --}}
        <x-ndhu007.ndhu007></x-ndhu007.ndhu007>
    </div>

    <script src="{{ asset('js/outsources/jquery-3.6.0.min.js') }}"></script>
    <script src="{{ asset('js/outsources/bootstrapv51.min.js') }}"></script>
    <script src="{{ asset('js/index.js') }}"></script>
    
    {{$scriptsRes}}
</body>
</html>