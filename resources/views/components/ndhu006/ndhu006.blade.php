<div id="NDHU006_studentArea">
    <img style="width: 100%" src="/images/NDHU006.png" />
    <div class="NDHU006_stTop">
        <h3 class="textaligncenter" style="padding: 30px 0">小幫手專區</h3>
        <div class="NDHU006_LHContainer NDHU006_LHCTop">
            <x-ndhu006.little-helper title="新生報到" className="NDHU006_littleHelper" hrefsrc="https://rc135.ndhu.edu.tw/">
                <img class="NDHU006_badge" src="/images/flag.svg"/>
            </x-ndhu006.little-helper>
            <x-ndhu006.little-helper title="未來學生" className="NDHU006_littleHelper" hrefsrc="https://www.ndhu.edu.tw/p/412-1000-9758.php?Lang=zh-tw">                
                <img class="NDHU006_badge" src="/images/position.svg"/>
            </x-ndhu006.little-helper>
            <x-ndhu006.little-helper title="研究生" className="NDHU006_littleHelper" hrefsrc="">
                <img class="NDHU006_badge" src="/images/book.svg"/>                                   
            </x-ndhu006.little-helper>
            <x-ndhu006.little-helper title="畢業專題" className="NDHU006_littleHelper" hrefsrc="">
                <img class="NDHU006_badge" src="/images/window.svg"/>
            </x-ndhu006.little-helper>
            <x-ndhu006.little-helper title="比賽檢定" className="NDHU006_littleHelper" hrefsrc="">
                <img class="NDHU006_badge" src="/images/crown-vip.svg"/>
            </x-ndhu006.little-helper>
            <x-ndhu006.little-helper title="畢業校友" className="NDHU006_littleHelper" hrefsrc="">
                <img class="NDHU006_badge" src="/images/user-check.svg"/>
            </x-ndhu006.little-helper>
        </div>
    </div>
    <div class="NDHU006_stBtm">
        <h3 class="textaligncenter" style="padding-bottom: 5rem">學生專區</h3>
        <div class="NDHU006_LHContainer NDHU006_LHCBtm">
            <x-ndhu006.little-helper title="FaceTest" className="NDHU006_littleHelper" hrefsrc="">
                <img class="NDHU006_badge" src="/images/facebook.svg"/>
            </x-ndhu006.little-helper>
            <x-ndhu006.little-helper title="InstagramTest" className="NDHU006_littleHelper" hrefsrc="">
                <img class="NDHU006_badge" src="/images/instagram.svg"/>
            </x-ndhu006.little-helper>
        </div>        
    </div>
</div>