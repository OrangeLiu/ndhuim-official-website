<div class="NDHUIM_nav-tab">
    <h2>消息公告</h2>
    <ul class="nav nav-tabs" id="nav-tab" role="tablist">
        <li class="nav-item"><a data-bs-toggle="tab" class="nav-link active" href="#ptab1"
                aria-current="page">最新消息</a></li>
        <li class="nav-item"><a data-bs-toggle="tab" class="nav-link" href="#ptab2">演講訊息</a></li>
        <li class="nav-item"><a data-bs-toggle="tab" class="nav-link" href="#ptab3">招生訊息</a></li>
        <li class="nav-item"><a data-bs-toggle="tab" class="nav-link" href="#ptab4">榮譽榜</a></li>
        <li class="nav-item"><a data-bs-toggle="tab" class="nav-link" href="#ptab5">課程消息</a></li>
    </ul>
</div>
<div class="tab-content">
    <div class="tab-pane fade show active" id="ptab1">
        <ul>
            @if(count($latestnews)>0)
            @foreach ($latestnews as $post)
            <li><a href={{$post->Url}}>{{ $post->Name }}</a></li>
            @endforeach
            @else
            <h4>目前沒有文章哦!</h4>
            @endif
        </ul>
        <a href={{url('category/hpdetails/ndhu_latestnews')}}>More</a>
    </div>
    <div class="tab-pane fade in" id="ptab2">
        <ul>
            @foreach ($speechPosts as $post)
            <li><a href={{$post->Url}}>{{ $post->Name }}</a></li>
            @endforeach
        </ul>
        <a href={{url('category/hpdetails/ndhu_speech')}}>More</a>
    </div>
    <div class="tab-pane fade in" id="ptab3">
        <ul>
            @foreach ($enrollment as $post)
            <li><a href={{$post->Url}}>{{ $post->Name }}</a></li>
            @endforeach
        </ul>
        <a href={{url('category/hpdetails/ndhu_enrollment')}}>More</a>
    </div>
    <div class="tab-pane fade" id="ptab4">
        <ul>
            @foreach ($honor as $post)
            <li><a href={{$post->Url}}>{{ $post->Name }}</a></li>
            @endforeach
        </ul>
        <a href={{url('category/hpdetails/ndhu_honor')}}>More</a>
    </div>
    <div class="tab-pane fade" id="ptab5">
        <ul>
            @foreach ($course as $post)
            <li><a href={{$post->Url}}>{{ $post->Name }}</a></li>
            @endforeach
        </ul>
        <a href={{url('category/hpdetails/ndhu_course')}}>More</a>
    </div>
</div>