<x-app-layout>
    <x-slot name="headRes">        
        <link rel="stylesheet" href="{{ asset('css/NDHUUTILS/NDHUUTILS_001008.css') }} " />
        </x-slot>
        
        <div>
            <div>
                <h2>CATEGORIES</h2>
                {{-- 分類們 --}}
                <ul class="NDHUUTILS_choser">
                    @foreach ($categories as $c)
                    <li><a class="NDHUUTILS_choserActive" 
                        @guest href={{url($c->Url)}}@endguest
                        @auth href={{url('dashboard/'.$c->Url)}}@endauth>
                        {{$c->category}}</a>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div>
                <h2>{{$categoryName_ch}}</h2>
                @auth
                @if($tableName != "ndhu_latestnews")
                <p><a href={{url('dashboard/add-post/' . $tableName)}}>新增文章</a></p>
                <p><a href={{url('dashboard/add-post-outer/' . $tableName)}}>新增外部文章</a></p>
                @endauth
                @endif
                <div>
                    @if(count($posts)>0)
                    @foreach ($posts as $p)
                    <a href={{$p->Url}}>{{$p->Name}}</a>
                    @auth
                    @if($tableName != "ndhu_latestnews")
                    <ul>           
                        <li><a                            
                            @if($p->isLink == 1)
                            href={{(url(Str::of('dashboard/edit-post-outer/'.$tableName.'/'.$p->randomPostId)->trim()).'?islink='.$p->isLink.'&edit=true')}}>修改</a>
                            @elseif($p->isLink == 0)
                            href={{(url(Str::of('dashboard/edit-post/'.$tableName.'/'.$p->randomPostId)->trim()).'?islink='.$p->isLink.'&edit=true')}}>修改</a>
                            @endif
                        </li>
                        <li>
                            <form onsubmit="return confirm('確定要刪除?')"
                                action={{url('dashboard/delete-post/'.$tableName.'/'.$p->randomPostId)}} method="post">
                                {{-- <input type="hidden" name="_method" value="DELETE"> --}}
                                @method('DELETE')
                                {{-- <input type="hidden" name="_token" value="{{csrf_token()}}"> --}}
                                @csrf
                                <button id="postDeleteBtn" type="submit">刪除</button>
                            </form>
                        </li>
                    </ul>
                    @endif
                    @endauth
                    @endforeach
                    @else
                    <h4>目前沒有文章哦</h4>
                    @endif
                </div>

    <x-slot name="scriptsRes">
        </x-slot>
</x-app-layout>