<footer class="NDHU007_footer">
    <div class="NDHU007_footerBkg">
        <div class="NDHU007_footerChild1">
            <p>國立東華大學資訊管理學系暨研究所</p>
            <p>Department of Information Management,</p>
            <p>National Dong Hwa University</p>
        </div>
        <div class="NDHU007_footerChild2">
            <p>地址：974301 花蓮縣壽豐鄉大學路二段1號 管理學院A206</p>
            <p>電話：(03)890-3000 轉 3102 / 傳真：(03)890-0153</p>
            <p>
                Address: No.1, Sec. 2, Da Hsueh Rd., Shoufeng, Hualien 974301, Taiwan,
                R.O.C.
            </p>
            <p>Tel: +886-3-890-3000 #3102 / FAX: +886-3-890-0153</p>
            <p>Copyright © 2023 Department of Information Management, National Dong Hwa University.</p>
        </div>        
    </div>
</footer>