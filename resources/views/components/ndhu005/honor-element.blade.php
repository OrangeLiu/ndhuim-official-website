<div id="NDHU005_honor">
    {{-- <img style="width: 100%" src="/images/Group 1.png"> --}}
    <h1 class="NDHU005_honorTitle">~榮譽榜~</h1>
    <div class="NDHU005_owlContainer">
        <div class="owl-carousel owl-theme">
            @for ($i = 0; $i < count($honorPosts); $i+=3)
            @if ($i < count($honorPosts))
            <div class="NDHU005_owlElement">
                <img class="NDHU005_owlElementImg owl-lazy" data-src="/images/198006140.jpg">
                <p>{{$honorPosts[$i]->Name}}</p>
            </div>
            @endif
            @if ($i + 1 < count($honorPosts))
            <div class="NDHU005_owlElement">
                <img class="NDHU005_owlElementImg owl-lazy" data-src="/images/198006140.jpg">
                <p>{{$honorPosts[$i + 1]->Name}}</p>
            </div>
            @endif
            @if ($i + 2 < count($honorPosts))
            <div class="NDHU005_owlElement">
                <img class="NDHU005_owlElementImg owl-lazy" data-src="/images/198006140.jpg">
                <p>{{$honorPosts[$i + 2]->Name}}</p>
            </div>
            @endif            
            @endfor
            {{-- <div class="NDHU005_owlElement">
                <img class="NDHU005_owlElementImg owl-lazy" data-src="/images/198006140.jpg">
                <p>【恭賀】本系劉英和老師指導學生參加「2021資料創新應用競賽」榮獲金獎</p>
            </div>
            <div class="NDHU005_owlElement">
                <img class="NDHU005_owlElementImg owl-lazy" data-src="/images/109748950.jpg">
                <p>【恭賀】本系侯佳利老師指導學生參加「2021 E化系統創意應用競賽」榮獲佳作</p>
            </div>
            <div class="NDHU005_owlElement">
                <img class="NDHU005_owlElementImg owl-lazy" data-src="/images/103790911.jpg">
                <p>【恭賀】本系侯佳利老師榮獲中華企業資源規劃學會頒發109年度E化教學優良獎</p>
            </div>
            <div class="NDHU005_owlElement">
                <img class="NDHU005_owlElementImg owl-lazy" data-src="/images/723748831.jpg">
                <p>恭賀國立東華大學資訊管理系數位內容實驗室(大學部學生，邱維揚；Wei-Yang Chiu)與中央電機系MINE實驗室所組成之團隊NCUEE於IEEE Brain Computer Interface Hackathon – 2016(San Diego, California, USA)榮獲Honorable Mention Award。</p>
            </div> --}}
        </div>
        <a class="NDHU005_honorMore" href={{url('/category/hpdetails/ndhu_honor')}}>More</a>    
    </div>
</div>