<x-app-layout>
    <x-slot name="headRes">
    <link rel="stylesheet" href="{{ asset('css/NDHU011/NDHU011.css') }} " />
    <link rel="stylesheet" href="{{ asset('css/NDHUUTILS/NDHUUTILS_001008.css') }} " />
    </x-slot>

        {{-- 師資介紹的 badge --}}
        <div class="NDHUUTILS_badgeContainer">
            <img id="NDHUUTILS_badge" src="/images/head20170625_01.jpg">
        </div>
        {{-- 專任教師等等 --}}
        @auth
        <div>
            <a href="/dashboard/editaboutus/aboutus/{{$category}}">修改</a>
        </div>
        @endauth
        <div class="NDHU011_aboutContainer">
            <ul class="NDHUUTILS_choser">
                <li><a @class(['NDHUUTILS_choserActive' => true])  
                    @guest href={{url('/aboutus/aboutus')}} @endguest 
                    @auth href={{url('/dashboard/aboutus/aboutus')}} @endauth>本系沿革</a></li>
                <li><a @class(['NDHUUTILS_choserActive' => false]) 
                    @guest href={{url('/aboutus/resources')}} @endguest
                    @auth href={{url('/dashboard/aboutus/resources')}} @endauth>設備資源</a></li>
                <li><a @class(['NDHUUTILS_choserActive' => false]) 
                    @guest href={{url('/aboutus/proficiency')}} @endguest
                    @auth href={{url('/dashboard/aboutus/proficiency')}} @endauth>專業能力</a></li>
                <li><a @class(['NDHUUTILS_choserActive' => false]) 
                    @guest href={{url('/aboutus/develop')}} @endguest
                    @auth href={{url('/dashboard/aboutus/develop')}} @endauth>發展重點</a></li>
                <li><a @class(['NDHUUTILS_choserActive' => false]) 
                    @guest href={{url('/aboutus/future')}} @endguest
                    @auth href={{url('/dashboard/aboutus/future')}} @endauth>升學重點</a></li>
            </ul>
            <div class="NDHU011_aboutContent">
                <div class="NDHUUTILS_title fw-bold">本系導覽</div>
                
                {{-- if route is aboutusEdit --}}
                @if(Route::is('getEditAboutusContent'))
                @auth
                <x-ndhuckeditor.ndhuckeditor :editorContent="$textarea"></x-ndhuckeditor.ndhuckeditor>
                @endauth
                @endif
                                  
                <div id="NDHU011_aboutContentContainer"></div>

                {{-- <h5 class="fw-bold">設立宗旨</h5>
                <div>
                    <p class="NDHU011_aboutArticle">資訊管理學系設立於民國九十年，正值我國資訊產業快速高度發展之際，企業界對其資訊化的需求極為殷切。本系以專業的師資與學生優良的團隊精神，培育專業與敬業樂群精神的人才，激發學生高度創新與思考的能力，以面對未來多元化的社會，為國家、社會或企業注入進步發展的人力資源。</p>
                    <p class="NDHU011_aboutArticle">自93 學年度起於企研所設立資管組碩士班；並自 94 學年度設立博士班；96學年度設立數位知識管理碩士班。為因應未來之趨勢，以數位內容與知識管理為核心，著重電子商務、產業電子化、及資訊應用之教學與研究，提供師生優良的e化教學環境，並支援東部大專院校相關教學所需，進而協助東部地區的企業或機構提升資訊化的能力，促進國家整體性與區域性的發展。</p>
                    <p class="NDHU011_aboutArticle">配合國家重點發展計畫，培育高級資訊科技人才 國家發展重點計畫之十大子計畫中，包括「培育E世代人才」、「國際創新研發基地」、「產值高業化」、「數位台灣」、「營運總部」、「全島運輸骨幹整建」皆以資訊管理應用人才之培育為主軸，積極培育高級資訊經理人已刻不容緩，也應是我國提升競爭力的重點計畫。</p>
                    <p class="NDHU011_aboutArticle">整合資訊與管理相關系所，發揮學域整合綜效 本校作為東部地區的網路中心，資訊管理學系之教學與研究的目標在於塑造並發揚資訊管理跨學域的特色，配合不同學程之課程規劃與學習特色，期冀落實學術理論與實務應用相互印證之教學目標。</p>
                    <p class="NDHU011_aboutArticle">培育東部地區高級資訊管理人才，平衡東西教育之發展 東部地區目前僅有東華大學、宜蘭大學、台東大學等培育少數之高級資訊科技人力，對於東部地區產業之發展略顯不足，本系提供師生優良的e化教學環境，並支援東部大專院校相關教學所需，進而協助東部地區的企業或機構提升資訊化的能力，促進國家整體性與區域性的平衡發展。</p>                   
                </div>                
                <h5 class="fw-bold">教育目標</h5>
                <div>
                    <ul>
                        <li>1. 培育具備團隊合作、創造與思考以及國際視野能力之人才。</li>
                        <li>2. 培育具備資訊專業知識與技術能力之人才。</li>
                        <li>3. 培育具備商業管理知識與應用能力之人才。</li>
                        <li>4. 培育具備整合資訊科技與管理知識之專業人才。 </li>
                    </ul>
                </div> --}}
            </div>
        </div>        

    <x-slot name="scriptsRes">
        <script src="https://cdn.ckeditor.com/ckeditor5/36.0.1/classic/ckeditor.js"></script>       
        <script src="{{ asset('js/NDHU011/NDHU011.js') }}"></script>     
        @if(Route::is('getEditAboutusContent'))
        <script src="{{ asset('js/NDHUUTILS/NDHUUTILS_ckeditor.js') }}"></script>
        @endif
    </x-slot>
</x-app-layout>