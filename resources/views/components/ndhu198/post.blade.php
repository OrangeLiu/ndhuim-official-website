{{-- ndhu198 --}}
<x-article-template>
    <x-slot name="headRes">
        <link rel="stylesheet" href="{{ asset('css/NDHU198/NDHU198.css') }} " />
    </x-slot>
    {{-- 內文透過 NDHU198.js 發送 request 取得，為了避免寫一堆 php 
        在前端，所以用 ajax(fetch) 方式 --}}
    <div class="NDHU198_postbox">        
        <h2 id="NDHU198_postTitle"></h2>
        <hr>
        <span>發佈日期: </span>
        <span>{{$postCreateDate[0]->createDate}}</span>
        <div id="NDHU198_postContainer" style="white-space: pre-line"></div>
        <p id="NDHU198_fileArea" style="white-space: pre-line">
        <div class="NDHU198_sharebox">

        </div>
    </div>

    <x-slot name="scriptsRes">
        <script src="{{ asset('js/NDHU198/NDHU198.js') }}"></script>
        </x-slot>
</x-article-template>