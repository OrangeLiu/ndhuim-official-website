{{-- 系所介紹 師資介紹 課程與獎學金 在校生專區 五年學碩 學術研究與成果 --}}
<header @class(['NDHU001_header','NDHU001_header_isHome' => Route::is('homepage')])>
    <div>
        <h1 id="NDHU001_logo"><a href={{ url('/') }}>NDHUIM</a></h1>
    </div>    
    <nav class="NDHU001_nav NDHUIM_nav-bar">
        <div class="NDHU001_searchNav">
            <form id="NDHU001_searchForm" action={{ url('/search') }} method="get">
                @csrf
                <input id="NDHU001_searchBar" type="text" name="s_t" placeholder="本站搜尋" />
                <input style="display: none;" type="submit" name="search" value="Search" />
            </form>
            <a href="https://www.ndhu.edu.tw/">國立東華大學首頁</a>
            <a href="#">English</a>
        </div>
        <ul class="nav-parent-headbar">
            <li><a id="l1" href=@auth {{ url('/dashboard') }}
                    @endauth
                    @guest
                    {{ url('/') }}
                    @endguest>
                    首頁</a>
            </li>
            <li><a id="l2" href=@auth {{ url('/dashboard/aboutus/aboutus') }}
                    @endauth
                    @guest
                    {{ url('/aboutus/aboutus') }}
                    @endguest>
                    系所介紹</a>
            </li>
            <li><a id="l3" href=@auth {{url('/dashboard/staff/teachers')}}@endauth
                    @guest
                    {{ url('/staff/teachers') }}
                    @endguest>師資介紹</a></li>
                    
            <li><a id="l4" href="#">課程與獎學金</a></li>
            <li><a id="l5" href={{url('/category/students/informationsys_stu')}}>在校生專區</a></li>
            <li><a id="l6" href="#">五年學碩</a></li>
            <li><a id="l7" href="#">學術研究與成果</a></li>
            @guest
            <li><a href={{ url('/login') }}>登入</a></li>
            @endguest
            @auth
            <li style="color:white; font-weight:600">Hello,username</li>
            <li>
            <form action={{ url('/logout') }} method="POST">
                @csrf
                <button id="NDHU001_logoutBtn" type="submit">登出</button>
            </form>
            </li>
            @endauth
            {{-- <li><button id="l9" href="#">搜尋</button></li> --}}
        </ul>        
    </nav>
</header>