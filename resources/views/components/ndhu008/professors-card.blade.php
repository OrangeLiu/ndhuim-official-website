<div class="NDHU008_staffCard">
    {{-- <p>{{$name}}</p> --}}
    <div class="NDHU008_cardFirst">
        <img class="NDHU008_avatar" src={{$imgurl}}>
        <p class="fw-bold">{{$name}}</p>
        <p class="fw-bold">{{$position}}</p>
    </div>
    <div class="NDHU008_cardSec">
        <div>
            <p class="NDHU008_subTitle fw-bold">聯絡資訊</p>
            @if($lab != NULL)<p class="fw-bold">LAB: {{$lab}}</p>@endif
            @if($fax != NULL)<p class="fw-bold">FAX: {{$fax}}</p>@endif
            @if($tel != NULL)<p class="fw-bold">TEL: {{$tel}}</p>@endif
            @if($email != NULL)<p class="fw-bold">EMAIL: <a>{{$email}}</a></p>@endif
            @if($officehour != NULL)<p class="fw-bold">OFFICE HOUR: {{$officehour}}</p>@endif
        </div>
    </div>
    <div class="NDHU008_cardThrd">
        <div>
            <p class="NDHU008_subTitle fw-bold">學歷</p>
            <p class="fw-bold">{{$degree}}</p>
        </div>
        <div>
            <p class="NDHU008_subTitle fw-bold">專長</p>
            <p class="fw-bold">{{$expertise}}</p>
        </div>
    </div>            
    <div class="NDHU008_cardForth">
        <div>
            <p class="NDHU008_subTitle fw-bold">MORE</p>
            <a class="fw-bold" href={{$personalweb}}>個人網站</a>
        </div>
    </div>
</div>