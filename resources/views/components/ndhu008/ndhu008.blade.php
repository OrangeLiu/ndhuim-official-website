<x-app-layout>
    <x-slot name="headRes">
    <link rel="stylesheet" href="{{ asset('css/NDHU008/NDHU008.css') }} " />
    <link rel="stylesheet" href="{{ asset('css/NDHUUTILS/NDHUUTILS_001008.css') }} " />
    </x-slot>

    {{-- 師資介紹的 badge --}}
    <div class="NDHUUTILS_badgeContainer">
        <img id="NDHUUTILS_badge" src="/images/head20170625_01.jpg">
    </div>
    {{-- 專任教師等等 --}}
    <div class="NDHU008_staffsContainer">
        <ul class="NDHUUTILS_choser">
            <li><a @class(['NDHUUTILS_choserActive' => true]) href={{url('/staff/teachers')}}>專任師資</a></li>
            <li><a @class(['NDHUUTILS_choserActive' => false]) href={{url('/staff/honor_professors')}}>榮譽教授</a></li>
            <li><a @class(['NDHUUTILS_choserActive' => false]) href={{url('/staff/adjunct_professors')}}>兼任師資</a></li>
            <li><a @class(['NDHUUTILS_choserActive' => false]) href={{url('/staff/retired_professors')}}>退休師資</a></li>
            <li><a @class(['NDHUUTILS_choserActive' => false]) href={{url('/staff/officestaff')}}>行政人員</a></li>
        </ul>
        <div class="NDHU008_staffs">
            <div class="NDHUUTILS_title fw-bold">系所成員</div>
            {{-- 教師卡片 --}}
            @if(count($staffs) == 0)
            <h3 class="fw-bold">這裡目前沒有資料哦，有的話會更新!</h3>
            @else
            @foreach($staffs as $p)
            <x-ndhu008.professors-card 
            :name="$p->name"
            :imgurl="$p->imgUrl"
            :position="$p->position"
            :lab="$p->lab"
            :fax="$p->fax"
            :tel="$p->telephone"
            :email="$p->email"
            :officehour="$p->officeHour"
            :degree="$p->degree"
            :expertise="$p->expertise"
            :personalweb="$p->personalWebsite"
            :status="$p->status"
                ></x-ndhu008.professors-card>        
            @endforeach
            @endif                
        </div>
    </div>        

    <x-slot name="scriptsRes">
    </x-slot>
</x-app-layout>