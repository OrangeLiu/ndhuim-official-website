<!-- Carousel container -->
<div class="NDHU002_carousel">
    <div class="NDHU002_carousel_container NDHU002_carousel_first">
        <!-- Full-width images with description -->
        <div class="NDHU002_carousel_cover">
            <img class="NDHU002_carousel_fade" src="https://images.unsplash.com/photo-1606761568499-6d2451b23c66?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1548&q=80">
            <div class="NDHU002_carousel_description">
                Department of Information Management<br>
                資訊管理學系
            </div>
        </div>
      
        <div class="NDHU002_carousel_cover">
            <img class="NDHU002_carousel_fade" src="https://images.unsplash.com/photo-1603573355706-3f15d98cf100?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2058&q=80">
            <div class="NDHU002_carousel_description">Caption Two</div>
        </div>
      
        <div class="NDHU002_carousel_cover">
            <img class="NDHU002_carousel_fade" src="https://images.unsplash.com/photo-1583373834259-46cc92173cb7?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1548&q=80">
            <div class="NDHU002_carousel_description">Caption Three</div>
        </div>
      
        <!-- Next and previous buttons -->
        <div class="NDHU002_carousel_prev" onclick="plusSlides(-1)"">
            <p>&#10094;</p>
        </div>
        
        <div class="NDHU002_carousel_next" onclick="plusSlides(1)"> 
            <p>&#10095;</p>
        </div>
    
        <!-- The dots/circles -->
        <div class="NDHU002_carousel_allDots">
            <span class="NDHU002_carousel_dot" onclick="currentSlide(1)"></span>
            <span class="NDHU002_carousel_dot" onclick="currentSlide(2)"></span>
            <span class="NDHU002_carousel_dot" onclick="currentSlide(3)"></span>
        </div>
      </div>

      <div class="NDHU002_carousel_bg">
        <div class="NDHU002_carousel_bg_left">
            <p class="NDHU002_carousel_bg_N NDHU002_carousel_bg_solid">N</p>
            <p class="NDHU002_carousel_bg_D NDHU002_carousel_bg_hollow">D</p>
            <p class="NDHU002_carousel_bg_H NDHU002_carousel_bg_solid">H</p>
            <p class="NDHU002_carousel_bg_U NDHU002_carousel_bg_hollow">U</p>
        </div>
        <div class="NDHU002_carousel_bg_right">
            <p class="NDHU002_carousel_bg_I NDHU002_carousel_bg_solid">I</p>
            <p class="NDHU002_carousel_bg_M NDHU002_carousel_bg_hollow">M</p>
        </div>
    </div>
</div>