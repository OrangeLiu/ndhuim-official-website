const postContainer = document.querySelector("#NDHU198_postContainer");
const fileUploadArea = document.querySelector("#NDHU198_fileArea");
const postTitle = document.querySelector("#NDHU198_postTitle");
var url = new URL(window.location.href);
url = url.pathname.substring(url.pathname.indexOf('/',15),url.pathname.length);
resu = fetch('/getPostContent'+url,{
    method:"GET",
}).then(res => {
    return res.json();
}).then(res => {
    console.log(res);
    postTitle.innerHTML = res.title;
    postContainer.innerHTML = res.content;
    fileUploadArea.innerHTML = res.file;
});