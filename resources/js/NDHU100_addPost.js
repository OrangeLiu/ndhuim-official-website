const postTitleContainer = document.querySelector("#postTitleContainer");
const sendPostBtn = document.querySelector("#NDHUCKeditor_sendBtn");
// const addParagraphBtn = document.querySelector("#addParagraph");
// const addImgBtn = document.querySelector("#addImg");
// const textareaValue = document.querySelector("#textarea");
const postTitleBtn = document.querySelector("#postTitleBtn");
const postTitle = document.querySelector("#postTitleInput");
const addDocBtn = document.querySelector("#addDoc");
const fileUploadArea = document.querySelector("#fileUploadArea");

let paragraphNum = 1;
let panelNum = 1;
let imgNum = 1;
let docNum = 1;

let match = {};
let imageFiles = {};
let matchF = {};
let docFiles = {};
let imgfd = new FormData();
let docfd = new FormData();

const url = new URL(window.location.href);
const urlParams = url.searchParams;

const delUpFileHandler = (e) => {
    document.querySelector(`#${matchF[e.currentTarget.id][1]}`).remove();
    document.querySelector(`#${matchF[e.currentTarget.id][0]}`).remove();
    delete docFiles[matchF[matchF[e.currentTarget.id][1]]];
    delete matchF[matchF[e.currentTarget.id][1]];
    delete matchF[e.currentTarget.id];
    console.log(docFiles);
    console.log(matchF);
};

const loadDocFile = (e) => {
    const doc = document.querySelector(`#${matchF[e.currentTarget.id]}`);
    console.log(doc);
    console.log(matchF[e.currentTarget.id]);
    // Adoc.innerHTML = docFileBtn.files.item(0).name;
    doc.src = URL.createObjectURL(e.target.files[0]);
    docFiles[matchF[e.currentTarget.id]] = e.target.files[0];
    // console.log(e.target.files[0]);
    const date = new Date();
    const y = date.getFullYear();
    let tm = date.getMonth() + 1;
    const m = tm < 10 ? "0" + tm : tm;
    let td = date.getDate();
    const d = td < 10 ? "0" + td : td;
    doc.href =
        // IP +
        "/storage/posts/" +
        y +
        "-" +
        m +
        "-" +
        d +
        "/" +
        e.target.files[0].name;
    doc.innerHTML = e.target.files[0].name;
    console.log(docFiles);
};

const addDocHandler = () => {
    const docContainer = document.createElement("div");
    docContainer.id = `docC${docNum}`;
    const upFileDelBtn = document.createElement("button");
    upFileDelBtn.id = `DelupDoc${docNum}`;
    upFileDelBtn.innerText = "刪除";
    upFileDelBtn.addEventListener("click", delUpFileHandler);
    docContainer.appendChild(upFileDelBtn);

    const uploadFileInput = document.createElement("input");
    uploadFileInput.type = "file";
    uploadFileInput.name = `upDoc${docNum}`;
    uploadFileInput.accept =
        ".doc,.docx,application/pdf,application/vnd.ms-excel,.csv,.xls,.pdf";
    uploadFileInput.id = `upDoc${docNum}`;
    uploadFileInput.addEventListener("change", loadDocFile);

    const docA = document.createElement("a");
    docA.href = docA.src;
    docA.id = `Adoc${docNum}`;
    docA.innerHTML = "請選擇文件";
    fileUploadArea.appendChild(docA);

    docFiles[docA.id] = null;

    docContainer.appendChild(uploadFileInput);
    docContainer.appendChild(docA);

    fileUploadArea.appendChild(docContainer);

    matchF[uploadFileInput.id] = `Adoc${docNum}`;
    matchF[upFileDelBtn.id] = [
        `docC${docNum}`,
        `upDoc${docNum}`,
        // `file${docNum}`,
    ];
    console.log(docFiles);
    docNum += 1;
};

const setPostTitleHandler = () => {
    let title;
    if (document.querySelector("#title") !== null) {
        title = document.querySelector("#title");
    } else {
        title = document.createElement("h2");
        title.id = "title";
        postTitleContainer.prepend(title);
    }
    title.innerText = postTitle.value;
};

// const modifyParagraphHandler = (e) => {
//     const tempObj = document.querySelector(`#${match[e.currentTarget.id][1]}`);
//     textareaValue.value = tempObj.innerHTML;
//     console.log(e.currentTarget.id);
//     addParagraphBtn.innerHTML = "修改段落";
//     addParagraphBtn.removeEventListener("click", addParagraphHandler);
//     addParagraphBtn.addEventListener("click", function handler() {
//         tempObj.innerHTML = textareaValue.value;
//         textareaValue.value = "";
//         addParagraphBtn.innerHTML = "新增段落";
//         this.removeEventListener("click", handler);
//         addParagraphBtn.addEventListener("click", addParagraphHandler);
//     });
// };

// const deleteParagraphHandler = (e) => {
//     document.querySelector(`#${match[e.currentTarget.id][0]}`).remove();
//     document.querySelector(`#${match[e.currentTarget.id][1]}`).remove();
//     delete match[e.currentTarget.id];
// };

// const modifyPostInit = () => {
//     if (urlParams.get("edit") !== "true") return;
//     else {
//         const modifyAndDels = document.querySelectorAll('[id^="md"]');
//         let ts = modifyAndDels[modifyAndDels.length - 1].id;
//         paragraphNum = ts.substring(ts.lastIndexOf("d") + 1, ts.length);
//         paragraphNum = parseInt(paragraphNum) + 1;
//         panelNum = paragraphNum;
//         // debugger;
//         for (let i = 0; i < modifyAndDels.length; i++) {
//             const tempindex = modifyAndDels[i].id.substring(2, 3);
//             const modifyBtn = document.querySelector(`#m${tempindex}`);
//             const paragraph = document.querySelector(`#p${tempindex}`);
//             const delBtn = document.querySelector(`#d${tempindex}`);
//             match[modifyBtn.id] = [modifyAndDels[i].id, paragraph.id];
//             match[delBtn.id] = [modifyAndDels[i].id, paragraph.id];
//             modifyBtn.addEventListener("click", modifyParagraphHandler);
//             delBtn.addEventListener("click", deleteParagraphHandler);
//             //上船文件也需補上
//         }
//     }
// };
// modifyPostInit();

// const deleteImgHandler = (e) => {
//     document.querySelector(`#${match[e.currentTarget.id][1]}`).remove();
//     document.querySelector(`#${match[e.currentTarget.id][0]}`).remove();
//     console.log(match[e.currentTarget.id][1]);
//     console.log(match[e.currentTarget.id][0]);
//     delete imageFiles[match[match[e.currentTarget.id][2]]];
//     delete match[match[e.currentTarget.id][2]];
//     delete match[e.currentTarget.id];
//     console.log(imageFiles);
//     console.log(match);
// };

const loadFile = (e) => {
    const image = document.querySelector(`#${match[e.currentTarget.id]}`);
    image.src = URL.createObjectURL(e.target.files[0]);
    imageFiles[match[e.currentTarget.id]] = e.target.files[0];
    console.log(imageFiles);
};

// const modifyPostImgInit = () => {
//     if (urlParams.get("edit") !== "true") return;
//     else {
//         const imgCs = document.querySelectorAll('[id^="imgC"]');
//         if (imgCs.length === 0) return;
//         let ts = imgCs[imgCs.length - 1].id;
//         imgNum = ts.substring(ts.lastIndexOf("C") + 1, ts.length);
//         imgNum = parseInt(imgNum) + 1;

//         for (let i = 0; i < imgCs.length; i++) {
//             const tempIndex = imgCs[i].id.substring(4, 5);
//             const delBtn = document.querySelector(`#dimg${tempIndex}`);
//             const imgInput = document.querySelector(`#file${tempIndex}`);
//             const uploadImg = document.querySelector(`#output${tempIndex}`);

//             //需處理若要修改文章如何取得原本的檔案
//             imageFiles[uploadImg.id] = imgInput.files[0];
//             // debugger;
//             delBtn.addEventListener("click", deleteImgHandler);
//             imgInput.addEventListener("change", loadFile);
//             match[imgInput.id] = `output${tempIndex}`;
//             match[delBtn.id] = [
//                 `imgC${tempIndex}`,
//                 `img${tempIndex}`,
//                 `file${tempIndex}`,
//             ];
//         }
//     }
// };
// modifyPostImgInit();

const modifyDocPostInit = () => {
    if (urlParams.get("edit") !== "true") return;
    else {
        const docs = document.querySelectorAll('[id^="docC"]');
        console.log(docs);
        if (docs.length === 0) return;
        let ts = docs[docs.length - 1].id;
        docNum = ts.substring(ts.lastIndexOf("C") + 1, ts.length);
        docNum = parseInt(docNum) + 1;

        for (let i = 0; i < docs.length; i++) {
            const tempIndex = docs[i].id.substring(4, 5);
            console.log(tempIndex);
            const docFileBtn = document.querySelector(`#upDoc${tempIndex}`);
            const delUpDocBtn = document.querySelector(`#DelupDoc${tempIndex}`);
            const Adoc = document.querySelector(`#ADoc${tempIndex}`);
            console.log(docFileBtn);

            docFiles[`files${i + 1}`] = null;

            docFileBtn.addEventListener("change", loadDocFile);

            matchF[docFileBtn.id] = `Adoc${tempIndex}`;
            matchF[delUpDocBtn.id] = [
                `docC${tempIndex}`,
                `upDoc${tempIndex}`,
                // `file${docNum}`,
            ];
        }
    }
};
modifyDocPostInit();

// const addParagraphHandler = () => {
//     const modifyAndDel = document.createElement("div");
//     const modifyBtn = document.createElement("button");
//     const delBtn = document.createElement("button");

//     modifyAndDel.id = `md${paragraphNum}`;
//     modifyBtn.innerText = "修改";
//     modifyBtn.id = `m${paragraphNum}`;
//     delBtn.id = `d${paragraphNum}`;
//     delBtn.innerText = "刪除";

//     modifyAndDel.appendChild(modifyBtn);
//     modifyAndDel.appendChild(delBtn);

//     const paragraph = document.createElement("p");
//     paragraph.id = `p${paragraphNum}`;
//     paragraph.classList.add("NDHU011_aboutArticle");
//     paragraph.innerText = textareaValue.value;

//     match[modifyBtn.id] = [modifyAndDel.id, paragraph.id];
//     match[delBtn.id] = [modifyAndDel.id, paragraph.id];

//     modifyBtn.addEventListener("click", modifyParagraphHandler);
//     delBtn.addEventListener("click", deleteParagraphHandler);

//     postContainer.appendChild(modifyAndDel);
//     postContainer.appendChild(paragraph);
//     textareaValue.value = "";
//     paragraphNum += 1;
//     panelNum += 1;
// };

// const addImgHandler = () => {
//     const imgContainer = document.createElement("div");
//     imgContainer.id = `imgC${imgNum}`;
//     const delBtn = document.createElement("button");
//     delBtn.id = `dimg${imgNum}`;
//     delBtn.innerText = "刪除";
//     delBtn.addEventListener("click", deleteImgHandler);
//     imgContainer.appendChild(delBtn);

//     const imgInput = document.createElement("input");
//     imgInput.type = "file";
//     imgInput.name = `img${imgNum}`;
//     imgInput.accept = "image/*";
//     imgInput.style.display = "none";
//     imgInput.id = `file${imgNum}`;
//     imgInput.addEventListener("change", loadFile);

//     const uploadPara = document.createElement("p");
//     uploadPara.id = `imgP${imgNum}`;
//     const uploadLabel = document.createElement("label");
//     uploadLabel.id = `lb${imgNum}`;
//     uploadLabel.style.cursor = "pointer";
//     uploadLabel.htmlFor = `file${imgNum}`;
//     uploadLabel.innerText = "Upload Image";
//     uploadPara.appendChild(uploadLabel);

//     const imgholderP = document.createElement("p");
//     imgholderP.id = `img${imgNum}`;
//     const uploadImg = document.createElement("img");
//     uploadImg.id = `output${imgNum}`;
//     uploadImg.style.width = "50%";
//     imgholderP.appendChild(uploadImg);

//     imageFiles[uploadImg.id] = null;

//     imgContainer.appendChild(imgInput);
//     imgContainer.appendChild(uploadPara);
//     // imgContainer.appendChild(imgholderP);

//     postContainer.appendChild(imgContainer);
//     postContainer.appendChild(imgholderP);

//     match[imgInput.id] = `output${imgNum}`;
//     match[delBtn.id] = [`imgC${imgNum}`, `img${imgNum}`, `file${imgNum}`];
//     // console.log(match);
//     console.log(imageFiles);
//     imgNum += 1;
// };

const apiPostSendHandler = async () => {
    // let imgfd = new FormData();
    const s = window.location.href;
    // postContainer.innerHTML = postContainer.innerHTML.replaceAll("\n", "");
    fileUploadArea.innerHTML = fileUploadArea.innerHTML
        .replaceAll("\n", "")
        .trim();
    console.log(fileUploadArea.innerHTML);
    //設定送出圖片檔名的格式
    const date = new Date();
    const y = date.getFullYear();
    let tm = date.getMonth() + 1;
    const m = tm < 10 ? "0" + tm : tm;
    let td = date.getDate();
    const d = td < 10 ? "0" + td : td;

    // const imgList = document.querySelectorAll('[id^="output"]');
    // for (let i = 0; i < imgList.length; i++) {
    //     console.log(imageFiles[imgList[i].id]);
    //     imgfd.append(imgList[i].parentNode.id, imageFiles[imgList[i].id]);
    //     // debugger;
    //     if (imgfd.get(imgList[i].parentNode.id).name !== undefined) {
    //         imgList[i].src =
    //             // IP +
    //             "/storage/posts/" +
    //             y +
    //             "-" +
    //             m +
    //             "-" +
    //             d +
    //             "/" +
    //             imgfd.get(imgList[i].parentNode.id).name;
    //     }
    //     console.log(imgList[i].src);
    // }
    // console.log(...imgfd);
    //上傳文件
    const docList = document.querySelectorAll('[id^="upDoc"]');
    const AdocList = document.querySelectorAll('[id^="Adoc"]');
    for (let i = 0; i < docList.length; i++) {
        const tempIndex = docList[i].id.substring(5, 6);
        console.log(docFiles);
        console.log(docFiles[AdocList[i].id]);
        docfd.append(`files${i + 1}`, docFiles[AdocList[i].id]);
        // debugger;
        docList[i].src =
            // IP +
            "/storage/posts/" +
            y +
            "-" +
            m +
            "-" +
            d +
            "/" +
            docfd.get(`files${i + 1}`).name;
        console.log(docList[i].src);
    }
    //保存送出刪除元素前的html，以利修改文章
    // let before_desc = postContainer.innerHTML;
    let before_uploadFiles = fileUploadArea.innerHTML;
    // before_desc.replace("blob:", "");
    // console.log(before_desc);

    //將不該在文章瀏覽出現的元素刪除
    // const pList = document.querySelectorAll('[id^="p"]');
    // pList.forEach((ele) => {
    //     if (ele.innerHTML.trim() === "") ele.remove();
    // });
    // 若點了新增圖片但沒有選，送出前需刪除整塊元素
    // imgList.forEach((ele) => {
    //     if (ele.src.trim() === "") ele.parentNode.remove();
    // });
    // const delBtnList = document.querySelectorAll('[id^="dimg"]');
    // delBtnList.forEach((ele) => {
    //     ele.remove();
    // });
    // const imgPList = document.querySelectorAll('[id^="imgP"]');
    // imgPList.forEach((ele) => {
    //     ele.remove();
    // });

    const delList = document.querySelectorAll('[id^="md"]');
    delList.forEach((ele) => {
        ele.remove();
    });
    //若點了新增文件但沒有選，送出前須先刪除未用到之元素
    // docList.forEach((ele) => {
    //     if (ele.value === "") ele.parentNode.remove();
    // });
    const DelupDocList = document.querySelectorAll('[id^="DelupDoc"]');
    DelupDocList.forEach((ele) => {
        ele.remove();
    });
    const upDocList = document.querySelectorAll('[id^="upDoc"]');
    upDocList.forEach((ele) => {
        ele.remove();
    });
    AdocList.forEach((ele) => {
        console.log(ele.href);
        if (ele.innerHTML === "test") ele.parentNode.remove();
    });
    //送出文章內容

    try {
        let URL = "/dashboard/";
        // let URL = IP + "/dashboard/";
        if (urlParams.get("edit") === "true") {
            const temparr = s.split("/");
            //temparr[5] === tableName, temparr[6] === randomid
            URL += "edit-post/" + temparr[5] + "/" + temparr[6];
        } else {
            URL += "add-post" + s.substring(s.lastIndexOf("/"), s.length);
        }
        console.log(URL);
        const response = await fetch(URL, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                credentials: "same-origin",
                "X-CSRF-TOKEN": document
                    .querySelector('meta[name="csrf-token"]')
                    .getAttribute("content"),
            },
            body: JSON.stringify({
                title: postTitle.value,
                postContent: editor.getData(),
                isLink: 0,
                uploadFiles: fileUploadArea.innerHTML,
                before_uploadFiles: before_uploadFiles,
            }),
        });
        console.log(postContainer.innerHTML);
        if (!response.ok) throw new Error("something wrong");
        const data = await response.json();
        console.log(data);
    } catch (error) {}

    // const delImgList = document.querySelectorAll('[id^="imgC"]');
    // delImgList.forEach((ele) => {
    //     ele.remove();
    // });

    // if (imgList.length !== 0) {
    //設定要送出的圖片的路徑

    // try {
    //     const response = await fetch(
    //         // IP +
    //         "/dashboard/receiveImg/" + imgNum, {
    //         method: "POST",
    //         headers: {
    //             credentials: "same-origin",
    //             "X-CSRF-TOKEN": document
    //                 .querySelector('meta[name="csrf-token"]')
    //                 .getAttribute("content"),
    //         },
    //         body: imgfd, //取得表格內的內容
    //     });
    //     console.log(response);
    //     if (!response.ok) throw new Error("something wrong");
    //     const data = await response.json();
    //     console.log(data);
    // } catch (error) {}

    try {
        const response = await fetch(
            // IP +
            "/dashboard/postDocs/" + docNum,
            {
                method: "POST",
                headers: {
                    credentials: "same-origin",
                    "X-CSRF-TOKEN": document
                        .querySelector('meta[name="csrf-token"]')
                        .getAttribute("content"),
                },
                body: docfd,
            }
        );
        console.log(docfd);
        console.log(response);
        if (!response.ok) throw new Error("something wrong");
        const data = await response.json();
        console.log(data);
    } catch (error) {}
    // window.location.href = IP;
};

sendPostBtn.addEventListener("click", apiPostSendHandler);
// addImgBtn.addEventListener("click", addImgHandler);
// addParagraphBtn.addEventListener("click", addParagraphHandler);
postTitleBtn.addEventListener("click", setPostTitleHandler);
addDocBtn.addEventListener("click", addDocHandler);
