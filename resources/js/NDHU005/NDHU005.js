$(document).ready(function () {
    $(".owl-carousel").owlCarousel({
        items: 3,
        loop: true,
        autoplay: true,
        nav: false,
        autoplayTimeout: 3000,
        autoplayHoverPause: true,
        lazyLoad:true,
        lazyLoadEager:4,
        responsive: {
            0: {
                items: 1,
                stagePadding: 30,
            },
            600: {
                items: 1,
                stagePadding: 50,
            },
            1000: {
                items: 3,
                stagePadding: 70,
            },
            2000: {
                items: 3,
                stagePadding: 80,
            }
        },
    });
});
