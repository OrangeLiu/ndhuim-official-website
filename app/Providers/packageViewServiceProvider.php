<?php

namespace App\Providers;

use App\View\Components\ArticleTemplate;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use App\View\Components\test0724;
use App\View\Components\test0725;

class packageViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Blade::component('test0724', test0724::class);
        Blade::component('ArticleTemplate', ArticleTemplate::class);
    }
}
