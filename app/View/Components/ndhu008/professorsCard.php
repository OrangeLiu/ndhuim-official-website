<?php

namespace App\View\Components\ndhu008;

use Illuminate\View\Component;

class professorsCard extends Component
{
    public $name = "";
    public $position = "";
    public $lab = "";
    public $fax = "";
    public $tel = "";
    public $email = "";
    public $officehour = "";
    public $degree = "";
    public $expertise = "";
    public $personalweb = "";
    public $imgurl = "";
    public $status = "";
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct(
        $name
        ,$position, $lab, $fax, $tel, $email, $officehour,
        $degree, $expertise, $personalweb, $imgurl, $status
        )
    {
        $this->name = $name;
        $this->position = $position;
        $this->lab = $lab;
        $this->fax = $fax;
        $this->tel = $tel;
        $this->email = $email;
        $this->officehour = $officehour;
        $this->degree = $degree;
        $this->expertise = $expertise;
        $this->personalweb = $personalweb;
        $this->imgurl = $imgurl;
        $this->status = $status;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.ndhu008.professors-card');
    }
}
