<?php

namespace App\View\Components\ndhu005;

use Illuminate\View\Component;

class honorElement extends Component
{
    public $honorPosts;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($honorPosts)
    {
        //
        $this->honorPosts = $honorPosts;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        
        return view('components.ndhu005.honor-element');
    }
}
