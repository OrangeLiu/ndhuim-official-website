<?php

namespace App\View\Components\ndhu006;

use Illuminate\View\Component;

class LittleHelper extends Component
{
    public $title;
    public $className;
    public $hrefsrc;
    // public $svgWidth;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($title, $className, $hrefsrc)
    {
        //
        $this->title = $title;
        $this->className = $className;
        $this->hrefsrc = $hrefsrc;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.ndhu006.little-helper');
    }
}
