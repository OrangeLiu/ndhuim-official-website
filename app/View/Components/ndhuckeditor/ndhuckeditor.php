<?php

namespace App\View\Components\ndhuckeditor;

use Illuminate\View\Component;

class ndhuckeditor extends Component
{
    public $editorContent;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($editorContent)
    {
        $this->editorContent = $editorContent;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.ndhuckeditor.ndhuckeditor');
    }
}
