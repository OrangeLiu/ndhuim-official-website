<?php

namespace App\View\Components\ndhu003;

use Illuminate\View\Component;

class navTab extends Component
{
    public $latestnews;
    public $speechPosts;
    public $enrollment;
    public $honor;
    public $course;
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($latestnews,$speechPosts,$enrollment,$honor,$course)
    {
        //
        $this->latestnews = $latestnews;
        $this->speechPosts = $speechPosts;
        $this->enrollment = $enrollment;
        $this->honor = $honor;
        $this->course = $course;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.ndhu003.nav-tab');
    }
}
