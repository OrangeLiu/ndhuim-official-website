<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TeacherInfo extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'lab',
        'fax',
        'telephone',
        'email',
        'officeHour',
        'degree',
        'expertise',
        'personalWebsite'
    ];
    
    protected $table = "ndhu_teacherInfo";
    protected $primarykey = 'teacher_id';
    public $incrementing = true;
}
