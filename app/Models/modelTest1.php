<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class modelTest1 extends Model
{
    use HasFactory;
    protected $table = 'modeltest1';
    public $timestamps = true;
}
