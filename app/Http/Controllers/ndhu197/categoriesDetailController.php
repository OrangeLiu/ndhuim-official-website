<?php

namespace App\Http\Controllers\ndhu197;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class categoriesDetailController extends Controller
{
    //
    function getCategoriesMorePosts(Request $req, $categoryBig, $categoryName)
    {
        // select 有哪些大分類
        $categories = DB::select('SELECT * FROM `' . $categoryBig . '`');
        // 每個子分類底下的文章
        // $currentUrl = isset(explode('/', url()->current())[4]) ? explode('/', url()->current())[4] : explode('/', url()->current())[3];
        $posts = DB::select('SELECT * FROM `' . $categoryName . '`');
        $categoryName_ch = $posts[0]->belongtotable_ch;
        $tableName = $posts[0]->belongtotable;
        return view('components.ndhu003_1.categoriesDetailLogin', ['posts' => $posts, 'categories' => $categories, 'categoryName_ch' => $categoryName_ch, 'tableName' => $tableName]);
    }
}
