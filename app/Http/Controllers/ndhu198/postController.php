<?php

namespace App\Http\Controllers\ndhu198;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use DOMDocument;

class postController extends Controller
{
    //
    function getPost(Request $req, $tableName, $postRandomId)
    {
        $postCreateDate = DB::select('SELECT `createDate` FROM `' . $tableName . '` WHERE randomPostId = ?', [$postRandomId]);
        return view('components.ndhu198.post', ['postCreateDate' => $postCreateDate]);
    }
    function getPostContent(Request $req, $tableName, $postRandomId) {
        $postContent = DB::select('SELECT * FROM `' . $tableName . '` WHERE randomPostId = ?', [$postRandomId]);
        $dom = new DOMDocument();
        $domContent = $dom->loadHTML($postContent[0]->description);
        return response()->json([
            "title" => $postContent[0]->Name,
            "content" => $postContent[0]->description,
            "file" =>$postContent[0]->uploadFiles
        ]);
    }
}
