<?php

namespace App\Http\Controllers\ndhu101;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Carbon;

use Illuminate\Http\Request;

class outerArticleManagerController extends Controller
{
    //
    public static $CATOGORIES_TITLE = [
        "ndhu_latestnews" => "最新消息",
        "ndhu_speech" => "演講公告",
        "ndhu_course" => "課務訊息",
        "ndhu_honor" => "榮譽榜",
        "ndhu_enrollment" => "招生訊息"
    ];
    function getAddPostOuter()
    {
        return view('admin/add-post-outer', ['linkTitle' => NULL, 'linkUrl' => NULL]);
    }
    function postAddPostOuter(Request $req, $tableName)
    {
        $postRandomId = Str::random(15);
        DB::insert(
            'INSERT INTO `' . $tableName . '`(`Url`, `Name`, `randomPostId`, `createDate`, `isLink`, `belongtotable`, `belongtotable_ch`) VALUES (?, ?, ?, ?, ?, ?, ?)',
            [
                json_decode($req->getContent())->{'linkUrl'},
                json_decode($req->getContent())->{'linkTitle'},
                $postRandomId,
                Carbon::now()->format('Y-m-d'),
                json_decode($req->getContent())->{'isLink'},
                $tableName,
                self::$CATOGORIES_TITLE[$tableName]
            ]
        );
        DB::insert(
            'INSERT INTO `ndhu_latestnews` (`Url`, `Name`, `randomPostId`, `createDate`, `isLink`, `belongtotable`, `belongtotable_ch`) VALUES (?, ?, ?, ?, ?, ?, ?)',
            [
                json_decode($req->getContent())->{'linkUrl'},
                json_decode($req->getContent())->{'linkTitle'},
                $postRandomId,
                Carbon::now()->format('Y-m-d'),
                json_decode($req->getContent())->{'isLink'},
                'ndhu_latestnews',
                '最新消息'
            ]
        );
        return response()->json(['ok' => 'postinsertsuccess']);
    }
    function getEditPostOuter(Request $req, $tableName, $postRandomId)
    {
        $postContent = DB::select('SELECT * FROM `' . $tableName . '` WHERE randomPostId = ?', [$postRandomId]);
        return view('admin/add-post-outer', ['linkTitle' => $postContent[0]->Name, 'linkUrl' => $postContent[0]->Url]);
    }
    function postEditPostOuter(Request $req, $tableName, $postRandomId)
    {
        $affected = DB::update('UPDATE `' . $tableName . '`SET Name = ?, Url = ? WHERE randomPostId = ?', [$req->linkTitle, $req->linkUrl, $postRandomId]);
        return response()->json(['ok' => 'postinsertsuccess']);
    }
}
