<?php

namespace App\Http\Controllers\ndhu100;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Carbon;
use Illuminate\Http\Request;

class articleManagerController extends Controller
{
    //
    public static $CATOGORIES_TITLE = [
        "ndhu_latestnews" => "最新消息",
        "ndhu_speech" => "演講公告",
        "ndhu_course" => "課務訊息",
        "ndhu_honor" => "榮譽榜",
        "ndhu_enrollment" => "招生訊息"
    ];
    //進入新增文章頁面
    function getAddPost($tableName)
    {
        // maybe some variable need to be passed..
        return view('admin/add-post', ['postDescBefore' => NULL, 'textarea' => NULL, 'postTitle' => NULL, 'uploadFiles' => NULL, 'before_uploadFiles' => NULL]);
    }
    //送出欲新增的文章
    function postAddPost(Request $req, $tableName)
    {
        // some variable need to be defined when request coming
        /*
            like req.title, req.image, req.description...
        */
        $postRandomId = Str::random(15);
        $postUrl = '/category/posts' . '/' . $tableName . '/' . $postRandomId;
        // insert into database
        // here Url need to be one of route to make href can link
        DB::insert(
            'INSERT INTO `' . $tableName . '`(`Url`, `Name`, `description`, `description_before`, `randomPostId`, `createDate`, `isLink`, `uploadFiles`, `before_uploadFiles`, `belongtotable`, `belongtotable_ch`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
            [
                $postUrl,
                json_decode($req->getContent())->{'title'},
                json_decode($req->getContent())->{'postContent'},
                NULL,
                $postRandomId,
                Carbon::now()->format('Y-m-d'),
                json_decode($req->getContent())->{'isLink'},
                json_decode($req->getContent())->{'uploadFiles'},
                json_decode($req->getContent())->{'before_uploadFiles'},
                $tableName,
                self::$CATOGORIES_TITLE[$tableName]
            ]
        );
        DB::insert(
            'INSERT INTO `ndhu_latestnews` (`Url`, `Name`, `description`, `description_before`, `randomPostId`, `createDate`, `isLink`, `uploadFiles`, `before_uploadFiles`, `belongtotable`, `belongtotable_ch`) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
            [
                $postUrl,
                json_decode($req->getContent())->{'title'},
                json_decode($req->getContent())->{'postContent'},
                NULL,
                $postRandomId,
                Carbon::now()->format('Y-m-d'),
                json_decode($req->getContent())->{'isLink'},
                json_decode($req->getContent())->{'uploadFiles'},
                json_decode($req->getContent())->{'before_uploadFiles'},
                'ndhu_latestnews',
                '最新消息'
            ]
        );
        return response()->json(['ok' => 'postinsertsuccess']);
    }
    //進入修改文章介面
    function getEditPost(Request $req, $tableName, $postRandomId)
    {
        $postContent = DB::select('SELECT * FROM `' . $tableName . '` WHERE randomPostId = ?', [$postRandomId]);
        if ($req->islink == 1 && $req->edit == true) {
            return view('admin/add-post', ['tableName' => $tableName, 'postTitle' => $postContent[0]->Name, 'textarea' => NULL, 'postRandomId' => $postRandomId, 'postLink' => $postContent[0]->Url]);
        } else {
            return view('admin/add-post', ['tableName' => $tableName, 'postTitle' => $postContent[0]->Name, 'textarea' => $postContent[0]->description, 'postRandomId' => $postRandomId, 'postDescBefore' => $postContent[0]->description_before, 'before_uploadFiles' => $postContent[0]->before_uploadFiles]);
        }
    }
    //送出修改好的文章
    function postEditPost(Request $req, $tableName, $postRandomId)
    {
        $affected = DB::update('UPDATE `' . $tableName . '` SET Name = ?, description = ?, description_before = ?, uploadFiles = ?, before_uploadFiles = ? WHERE randomPostId = ?', 
        [
            json_decode($req->getContent())->{'title'},
            json_decode($req->getContent())->{'postContent'},
            NULL,
            json_decode($req->getContent())->{'uploadFiles'},
            json_decode($req->getContent())->{'before_uploadFiles'},
            $postRandomId
        ]);
        $affected = DB::update('UPDATE `ndhu_latestnews` SET Name = ?, description = ?, description_before = ?, uploadFiles = ?, before_uploadFiles = ? WHERE randomPostId = ?', 
        [
            json_decode($req->getContent())->{'title'},
            json_decode($req->getContent())->{'postContent'},
            NULL,
            json_decode($req->getContent())->{'uploadFiles'},
            json_decode($req->getContent())->{'before_uploadFiles'},
            $postRandomId
        ]);
        return redirect()->route('dashboard');
    }
    function deletePost(Request $req, $tableName, $postRandomId)
    {
        $deleted = DB::delete('DELETE FROM `' . $tableName . '` WHERE randomPostId = ?', [$postRandomId]);
        $deleted = DB::delete('DELETE FROM `ndhu_latestnews` WHERE randomPostId = ?', [$postRandomId]);
        return redirect()->route('dashboard');
    }
}
