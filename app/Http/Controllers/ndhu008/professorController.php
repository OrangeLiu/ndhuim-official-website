<?php

namespace App\Http\Controllers\ndhu008;

use App\Http\Controllers\Controller;
use App\Models\ndhu008\TeacherInfo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class professorController extends Controller
{
    //
    function getStaffs(Request $req, $staffType){
        //取得教師資料
        //professors = db::select
        // $staffs = DB::table('teacher_infos')->get();
        if($staffType == "teachers") {
            $staffs = DB::table('teacher_infos')->where('status', '1')->get();
        } elseif($staffType == "honor_professors") {
            $staffs = DB::table('teacher_infos')->where('status', '2')->get();
        } elseif($staffType == "adjunct_professors") {
            $staffs = DB::table('teacher_infos')->where('status', '3')->get();
        } elseif($staffType == "retired_professors") {
            $staffs = DB::table('teacher_infos')->where('status', '4')->get();
        } elseif($staffType == "officestaff") {
            $staffs = DB::table('teacher_infos')->where('status', '5')->get();
        }
        //return blade
        return view('components.ndhu008.ndhu008', ['staffs' => $staffs, 'staffType' => $staffType]);
    }
}
