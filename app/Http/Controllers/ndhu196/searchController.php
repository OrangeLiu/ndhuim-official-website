<?php

namespace App\Http\Controllers\ndhu196;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class searchController extends Controller
{
    //
    function searchPosts(Request $req)
    {
        // search from all title
        $posts = DB::select('SELECT * FROM latestnews WHERE title LIKE \'%' . $req->input('s_t') . '%\'');
        // $currentUrl = explode('/', url()->current())[3];
        if (Auth::check()) {
            return view('dashboard', ['posts' => $posts]);
        } else if (!Auth::check()) {
            return view('homepage', ['posts' => $posts]);
        }
    }
}
