<?php

namespace App\Http\Controllers\ndhu011;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class aboutusController extends Controller
{
    function getAboutus(Request $req, $category) {
        return view('components.ndhu011.aboutus', ['textarea' => NULL, 'category' => $category]);
    }
    function getAboutusContent(Request $req, $category) {
        $aboutusContent = DB::table('ndhu011_about_contents')->where('category', $category)->first();
        return response()->json([
            "content" => $aboutusContent->content
        ]);
    }
    function geteditaboutus(Request $req, $category) {
        $aboutusContent = DB::table('ndhu011_about_contents')->where('category', $category)->first();
        if($aboutusContent->content == NULL) {
            $result = NULL;
        } else {
            $result = $aboutusContent->content;
        }
        return view('components.ndhu011.aboutus', ['textarea' => $result, 'category' => $category]);
    }
    function posteditaboutus(Request $req) {
        DB::table('ndhu011_about_contents')
        ->where('category', $req->category)
        ->update([
            'content' => json_decode($req->getContent())->{'postContent'},
        ]);
        return response()->json([
            "ok" => 'ok'
        ]);
    }
}
