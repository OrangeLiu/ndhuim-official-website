const linkInputCkBtn = document.querySelector("#linkInputCkBtn");
const linkInput = document.querySelector("#linkInput");
const linkTitle = document.querySelector("#linkTitle");

const IP = "http://127.0.0.1";

const url = new URL(window.location.href);
const urlParams = url.searchParams;

const sendLinkHandler = async () => {
    if (linkInput.value === null) return;

    const s = window.location.href;

    try {
        let URL = IP + "/dashboard/";
        if (urlParams.get("edit") === "true") {
            const temparr = s.split("/");
            URL += "edit-post-outer/" + temparr[5] + "/" + temparr[6];
        } else {
            URL += "add-post-outer" + s.substring(s.lastIndexOf("/"), s.length);
        }
        const response = await fetch(URL, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
                credentials: "same-origin",
                "X-CSRF-TOKEN": document
                    .querySelector('meta[name="csrf-token"]')
                    .getAttribute("content"),
            },
            body: JSON.stringify({
                linkTitle: linkTitle.value,
                linkUrl: linkInput.value,
                isLink: 1,
            }),
        });
        if (!response.ok) throw new Error("something wrong");
        const data = await response.json();
        console.log(data);
    } catch (error) {}

    // window.location.href = IP;

};

linkInputCkBtn.addEventListener("click", sendLinkHandler);
