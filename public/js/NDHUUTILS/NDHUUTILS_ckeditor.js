let editor;
class MyUploadAdapter {
    constructor(loader) {
        this.loader = loader;
    }
    upload() {
        return new Promise((resolve, reject) => {
            const reader = (this.reader = new window.FileReader());
            reader.addEventListener("load", () => {
                resolve({ default: reader.result });
            });
            reader.addEventListener("error", (err) => {
                reject(err);
            });
            reader.addEventListener("abort", () => {
                reject();
            });
            this.loader.file.then((file) => {
                reader.readAsDataURL(file);
            });
        });
    }
    abort() {
        this.reader.abort();
    }
}
function MyAdapterPlugin(editor){
    editor.plugins.get('FileRepository').createUploadAdapter = ( loader ) => { return new MyUploadAdapter( loader ) };
};

ClassicEditor.create(document.querySelector("#editor"), {
    // plugins: [IndentBlock, function(editor) {
    //     editor.keystroke.set('Tab', (data, cancel) => {
    //         const command = editor.commands.get( 'indentBlock' );

    //         if ( command.isEnabled ) {
    //             command.execute();
    //             cancel();
    //         }
    //     });
    // }]
    // plugins:[CKFinder],
    // toolbar:['ckfinder', 'uploadImage'],
    // extraPlugins: [MyAdapterPlugin],
    ckfinder: {
        options: {
            resourceType: "Images",
        },
        uploadUrl:
            "/ckfinder/connector?command=QuickUpload&type=Images&responseType=json",
    },
    // headers: {
    //     'X-CSRF-TOKEN': 'CSFR-Token',
    // }
})
    .then((edi) => {
        editor = edi;
    })
    .catch((error) => {
        console.error(error);
    });
