const NDHU001_header_isHome = document.querySelector(".NDHU001_header")

document.onscroll = () => {
    if(document.documentElement.scrollTop > 40) {
        NDHU001_header_isHome.style.backgroundColor = 'rgb(16, 23, 32)';
    } else {
        NDHU001_header_isHome.style.backgroundColor = 'transparent';    
    }
}