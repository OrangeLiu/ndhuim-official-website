let coverIndex = 1;
showSlides(coverIndex);

let coverTimer = undefined;

function setCoverTimer() {
  coverTimer = setInterval(function() {
    function plusCovers() {
      coverIndex += 1
      showSlides(coverIndex);
    }
    plusCovers();
    
  }, 8000);
}
setCoverTimer();

// Next/previous controls
function plusSlides(n) {
  showSlides(coverIndex += n);
  clearInterval(coverTimer);
  setCoverTimer();
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(coverIndex = n);
  clearInterval(coverTimer);
  setCoverTimer();
}

function showSlides(n) {
  let i;
  let covers = document.getElementsByClassName("NDHU002_carousel_cover");
  let dots = document.getElementsByClassName("NDHU002_carousel_dot");
  if (n > covers.length) {coverIndex = 1}
  if (n < 1) {coverIndex = covers.length}
  for (i = 0; i < covers.length; i++) {
    covers[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" NDHU002_carousel_active", "");
  }
  covers[coverIndex-1].style.display = "block";
  dots[coverIndex-1].className += " NDHU002_carousel_active";
}

